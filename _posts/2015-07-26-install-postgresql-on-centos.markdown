---
layout: post
title: "在CentOS6.6上安装PostgreSQL"
date: 2015-07-26 23:41:42 +0800
categories: postgresql
excerpt: CentOS上PostgreSQL的安装与配置方法
---

##### 在CentOS6.6上安装PostgreSQL

这篇博客介绍如何在CentOS6.6上安装和配置PostgreSQL，参考[http://www.postgresql.org/download/linux/redhat/](http://www.postgresql.org/download/linux/redhat/)<br/>

<br/>

> 安装PostgreSQL

+ 添加repository RPM
{% highlight console %}
yum install http://yum.postgresql.org/9.4/redhat/rhel-6-x86_64/pgdg-redhat94-9.4-1.noarch.rpm
{% endhighlight %}

+ 开始安装PostgreSQL相关package
{% highlight console %}
yum install postgresql94-server postgresql94-contrib
{% endhighlight %}

> 初始化及配置

{% highlight console %}
# 初始化数据库集群
service postgresql-9.4 initdb
# 开机运行
chkconfig postgresql-9.4 on
# 启动postgresql服务
service postgresql-9.4 start
# 建立链接
ln -s /usr/local/pgsql-9.4/bin/* /usr/bin/
{% endhighlight %}

+ 其它初始化操作
{% highlight console %}
# 切换到postgres
su postgres
# 创建数据库用户
createuser userxxx
# 创建数据库
createdb dbxxx
# 进入postgresql交互终端
psql
# 为userxxx设置密码
ALTER USER userxxx WITH ENCRYPTED PASSWORD 'passwordxxx';
# 为userxxx授权
GRANT ALL PRIVILEGES ON DATABASE dbxxx TO userxxx;
{% endhighlight %}

+ 修改pg_hba.conf
{% highlight console %}
gedit /var/lib/pgsql/9.3/data/pg_hba.conf
    # 修改密码传送类型
    # IPv4 local connections:
    host    all             all             127.0.0.1/32            md5
    # IPv6 local connections:
    host    all             all             ::1/128                 md5
{% endhighlight %}

> 与Django项目建立连接

+ 安装psycopg2
{% highlight console %}
pip install psycopg2
{% endhighlight %}

+ 配置Django项目settings.py
{% highlight python %}
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'dbxxx',
        'USER': 'userxxx',
        'PASSWORD': 'passwordxxx',
        'HOST': 'localhost',
        'PORT': '',
    }
}
{% endhighlight %}
