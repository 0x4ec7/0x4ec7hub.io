---
layout: post
title: "在CentOS6.6上安装Git Server"
date: 2015-07-27 21:30:07 +0800
categories: git git-server
excerpt: CentOS上Git以及Git Server的安装和配置方法
---

##### 在CentOS6.6上安装Git Server

这篇博客介绍如何在CentOS6.6上安装和配置Git Server<br/><br/>

> 安装git

{% highlight console %}
yum install curl-devel expat-devel gettext-devel openssl-devel zlib-devel perl-devel python-setuptools
wget https://www.kernel.org/pub/software/scm/git/git-2.4.6.tar.gz
tar -xzvf git-2.4.6.tar.gz
cd git-2.4.6
./configure --prefix=/usr/local/git
make && make install
{% endhighlight %}

> 添加用户

{% highlight console %}
mkdir /home/git
useradd git -d /home/git
passwd git
chown git:git /home/git
{% endhighlight %}

> 生成并上传公钥

{% highlight console %}
# 在开发机上生成公钥
ssh-keygen -t rsa
# 上传到git服务器
scp /root/.ssh/id_rsa.pub git@ipxxx:~
# 在服务器上查看公钥
cat /home/git/id_rsa.pub
{% endhighlight %}

> 安装配置gitosis

+ 安装gitosis
{% highlight console %}
cd /usr/local/git/share
git clone git@github.com:res0nat0r/gitosis.git
cd gitosis
python2 setup.py install
{% endhighlight %}

+ 生成gitosis管理库
{% highlight console %}
su git
gitosis-init < id_rsa.pub
chmod 755 /home/git/repositories
chmod 755 /home/git/repositories/gitosis-admin.git/hooks/post-update
{% endhighlight %}

+ Clone管理库到开发机
{% highlight console %}
git clone git@ipxxx:gitosis-admin.git
{% endhighlight %}

+ 配置用户权限

gitosis-admin文件夹中有gitosis.conf文件和keydir文件夹 \\
gitosis.conf是用来设置用户、仓库和权限的配置文件 \\
keydir用于存放用户的公钥 \\
以下是一个group的配置
{% highlight console %}
[group gitosis-admin]
members = userxxx
writable = gitosis-admin
{% endhighlight %}
第一行group后是group的名称\\
第二行是这个group的成员，如果有多个成员，用空格隔开\\
第三行是这个组的成员对于项目gitosis-admin所拥有的权限。writable表示可读可写，readonly表示只读\\
这整个group配置表示当前只有用户userxxx拥有项目gitosis-admin的读写权限
<br/>

在配置文件中可对用户编组
{% highlight console %}
[group common_developers]
members = user1 user2

[group website]
members   = @common_developers user3
writable  = website

[group app]
members   = @common_developers user4
writable  = app
{% endhighlight %}
以上配置表示user1、user2、user3同时开发website，对website有读写权限；user1、user2、user4同时开发app，对app有读写权限 \\
<br/>
gitosis.conf中的用户名来自keydir下的公钥名称，用户userxxx在keydir下保存的公钥名称是userxxx.pub

修改好后，将所做修改push到服务器，配置即可生效

> 安装gitweb

如果有运行中的apache server，可安装gitweb，实现浏览器访问git服务器

+ 安装gitweb
{% highlight console %}
yum install gitweb
{% endhighlight %}

+ 修改httpd.conf（可自由配置）
{% highlight xml %}
<Directory "/usr/local/git/share/gitweb">
    Options +FollowSymLinks +ExecCGI
    AddHandler cgi-script .cgi
    DirectoryIndex index.cgi gitweb.cgi
    Require all granted
</Directory>
{% endhighlight %}

+ 修改gitweb.conf
{% highlight console %}
# 将$projectroot修改为
$projectroot = "/home/git/repositories";
{% endhighlight %}

此时打开ipxxx/gitweb即可看到现有的repositories

+ 修改gitweb theme

从https://github.com/kogakure/gitweb-theme下载gitweb theme
替换/usr/local/git/share/gitweb/static中的相关文件

> 使用流程

+ 在服务器上生成空的仓库
{% highlight console %}
su git
git init --bare /home/git/repositories/test.git
{% endhighlight %}

+ 在gitosis-admin中新建组
{% highlight console %}
[group test]
members = userxxx
writable = test
{% endhighlight %}
修改后提交更新

+ 在开发机clone这个repository
{% highlight console %}
git clone git@ipxxx:test.git
{% endhighlight %}

或者
{% highlight console %}
mkdir test
cd test
git init
git remote add origin git@ipxxx:test.git
...
{% endhighlight %}
