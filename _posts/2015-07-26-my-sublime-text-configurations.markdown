---
layout: post
title: "Sublime Text 3配置"
date: 2015-07-26 15:30:17 +0800
categories: sublime
excerpt: 我在Windows上的Sublime Text 3的配置
---

##### Sublime Text 3配置

这篇博客介绍我在Windows上的ST3的配置。关于Sublime基础知识，可参考[http://www.cnblogs.com/figure9/p/sublime-text-complete-guide.html](http://www.cnblogs.com/figure9/p/sublime-text-complete-guide.html)<br/>

<br/>

> 安装并添加环境变量

+ 添加环境变量

将ST安装的根目录（C:\Program Files (x86)\Sublime Text 3）添加到系统变量Path中，之后可在命令行中直接操作Sublime
<table>
    <tr>
        <th>命令</th>
        <th>说明</th>
    </tr>
    <tr>
        <td>subl .</td>
        <td>打开当前目录</td>
    </tr>
    <tr>
        <td>subl file</td>
        <td>打开file文件</td>
    </tr>
    <tr>
        <td>subl folder</td>
        <td>打开folder文件夹</td>
    </tr>
</table>

> 常用Package

+ 安装Package Control
{% highlight console %}
ctrl + `
# 将以下内容粘贴到console中[https://packagecontrol.io/installation]
import urllib.request,os,hashlib; h = 'eb2297e1a458f27d836c04bb0cbaf282' + 'd0e7a3098092775ccb37ca9d6b2e4b7d'; pf = 'Package Control.sublime-package'; ipp = sublime.installed_packages_path(); urllib.request.install_opener( urllib.request.build_opener( urllib.request.ProxyHandler()) ); by = urllib.request.urlopen( 'http://packagecontrol.io/' + pf.replace(' ', '%20')).read(); dh = hashlib.sha256(by).hexdigest(); print('Error validating download (got %s instead of %s), please try manual install' % (dh, h)) if dh != h else open(os.path.join( ipp, pf), 'wb' ).write(by)
{% endhighlight %}

+ 安装其它常用Package

<table>
    <tr>
        <th>插件名</th>
        <th>说明</th>
        <th>默认快捷键</th>
    </tr>
    <tr>
        <td>AdvancedNewFile</td>
        <td>新建文件增强插件</td>
        <td>ctrl+alt+n</td>
    </tr>
    <tr>
        <td>Bracket Highlighter</td>
        <td>代码高亮插件</td>
        <td>/</td>
    </tr>
    <tr>
        <td>SyncedSideBar</td>
        <td>同步项目侧边栏</td>
        <td>/</td>
    </tr>
    <tr>
        <td>Emmet</td>
        <td>HTML/CSS代码编辑插件，<a href="//docs.emmet.io/cheat-sheet/" target="_blank">使用说明</a></td>
        <td>/</td>
    </tr>
    <tr>
       <td>Tag</td>
       <td>HTML/XML标签缩进、补全和校验</td>
       <td>ctrl+alt+f</td>
   </tr>
   <tr>
       <td>ColorPicker</td>
       <td>颜色选择插件</td>
       <td>ctrl+shift+c</td>
   </tr>
</table>

> 绑定快捷键

Preferences > Key Bindings - User
添加
{% highlight json %}
[
    { "keys": ["shift+tab"], "command": "reindent" , "args": { "single_line": false } },
    { "keys": ["ctrl+q"], "command": "run_macro_file", "args": {"file": "res://Packages/Default/Delete Line.sublime-macro"} }
]
{% endhighlight %}
<table>
    <tr>
        <th>快捷键</th>
        <th>说明</th>
    </tr>
    <tr>
        <td>shift+tab</td>
        <td>重新缩进所有行</td>
    </tr>
    <tr>
        <td>ctrl+q</td>
        <td>删除一行</td>
    </tr>
</table>

> 自定义插件（Plugin）

Tools > New Plugin...，将新建的文件保存为date_time.py（文件名格式为xxx.py）
{% highlight python %}
import sublime, sublime_plugin
import datetime

# 添加日期
class AddDateCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        self.view.run_command("insert_snippet", { "contents": "%s" %  datetime.datetime.now().strftime('%Y-%m-%d') } )

# 添加日期和时间
class AddTimeCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        self.view.run_command("insert_snippet", { "contents": "%s" %  datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') } )
{% endhighlight %}

然后绑定快捷键
{% highlight json %}
[
    { "keys": ["ctrl+shift+,"], "command": "add_date" },
    { "keys": ["ctrl+shift+."], "command": "add_time" }
]
{% endhighlight %}
<table>
    <tr>
        <th>快捷键</th>
        <th>说明</th>
    </tr>
    <tr>
        <td>ctrl+shift+,</td>
        <td>添加日期，格式为：2015-07-26</td>
    </tr>
    <tr>
        <td>ctrl+shift+.</td>
        <td>添加日期和时间，格式为：2015-07-26 09:59:00</td>
    </tr>
</table>
其它插件的编写参照以上步骤

> 自定义代码补全（Completion）

使用代码补全可以添加一些常用的特殊词句的快速补全

Preferences > Browse Packages...，打开User文件夹，新建Python.sublime-completions文件（文件名的格式为xxx.sublime-completions）
{% highlight json %}
{
    "scope": "source.python",
    "completions": [
        { "trigger": "i", "contents": "import $1" },
        { "trigger": "ia", "contents": "import $1 as $2" },
        { "trigger": "fi", "contents": "from $1 import $2" },
        { "trigger": "fia", "contents": "from $1 import $2 as $3" }
    ]
}
{% endhighlight %}
当在python文件中输入fi，再按下tab时，可以直接补全为from import，并且定位点在from后。输入第一个参数后，再按下tab，可快速定位到第二个参数，也就是import之后。按下ctrl+shift+alt+p可查看文件的scope。\\
其它的代码补全定义方法参考以上步骤。

> 自定义代码片段（Snippet）

Tools > New Snippet...，将新建的文件保存为Python-header.sublime-snippet（文件名格式为xxx.sublime-snippet）
{% highlight xml %}
<snippet>
    <content><![CDATA[
'''
Created on ${1}

@author 仇之东
'''

${2}
]]></content>
    <!-- Optional: Set a tabTrigger to define how to trigger the snippet -->
    <tabTrigger>pyhd</tabTrigger>
    <!-- Optional: Set a scope to limit where the snippet will trigger -->
    <scope>source.python</scope>
</snippet>
{% endhighlight %}
保存之后在python文件中输入pyhd，再按下tab就可以自动补全.py文件的header。\\
其它的代码片段的定义参考以上步骤。

<br/>
以上配置文件存放在[https://github.com/0x4ec7/SublimeConfig](https://github.com/0x4ec7/SublimeConfig)
