---
layout: post
title: "欢迎来到仇之东的个人博客"
date: 2015-07-22 21:48:24 +0800
categories: jekyll
style: "background: url('../static/img/bg.jpg'); background-position: 0 60%; background-size: 130% auto"
excerpt: 介绍这个博客的基本情况
---

##### 欢迎来到仇之东的个人博客！

这里主要介绍这个博客的搭建过程，我的个人信息，请移步[关于](/about)<br/>
本站源代码 [https://github.com/0x4ec7/0x4ec7.github.io](//github.com/0x4ec7/0x4ec7.github.io)

<br/>

> 配置开发机（Ubuntu）

+ 安装Jekyll
{% highlight console %}
sudo apt-get install ruby
sudo apt-get install ruby-dev
{% endhighlight %}

+ 更换gem源
{% highlight console %}
gem sources --remove https://rubygems.org/
gem sources -a https://ruby.taobao.org/
gem update --system
{% endhighlight %}

+ 安装Jekyll
{% highlight console %}
gem install jekyll
{% endhighlight %}

> 开发工作

+ 生成站点
{% highlight console %}
jekyll new 0x4ec7.github.io
cd 0x4ec7.github.io
# 使用开发服务器调试预览
jekyll serve
{% endhighlight %}

站点生成后开始开发工作，Jekyll使用说明[http://jekyllrb.com/](http://jekyllrb.com/)

+ 购买域名（可选）

购买域名之后将域名解析到0x4ec7.github.io，记录类型为CNAME，或者解析到192.30.252.153(192.30.252.154)，记录类型为A记录

+ 添加CNAME

在0x4ec7.github.io目录下新建文件CNAME，内容为购买的域名，不包含http前缀
{% highlight console %}
www.ialpha.me
{% endhighlight %}

> 发布网站

+ 在Github上新建repository

新建的repository的名称为username.github.io，这里是我的github账号名0x4ec7
{% highlight console %}
0x4ec7.github.io
{% endhighlight %}
新生成的repository的SSH链接为
{% highlight console %}
git@github.com:0x4ec7/0x4ec7.github.io.git
{% endhighlight %}

+ 初始化并添加远程仓库
{% highlight console %}
git init
git remote add origin git@github.com:0x4ec7/0x4ec7.github.io.git
{% endhighlight %}

+ 提交代码
{% highlight console %}
git add --all
git commit -m "first commit"
git push origin master
{% endhighlight %}

提交完成之后，打开[http://0x4ec7.github.io](http://0x4ec7.github.io)或者[http://www.ialpha.me](http://www.ialpha.me)就可以打开刚发布的网站

