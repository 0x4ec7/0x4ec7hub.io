---
layout: post
title: "在CentOS6.6上安装Django生产环境"
date: 2015-07-23 23:26:11 +0800
categories: django
excerpt: 介绍如何在CentOS上安装Django生产环境以及相关配置
---

##### 在CentOS6.6上安装Django生产环境

<br/>

> 安装依赖

+ 安装或更新gcc, g++
{% highlight console %}
yum install gcc
yum install gcc-g++
{% endhighlight %}

+ 安装ssl以及bzip2
{% highlight console %}
# ssl
yum install openssl-devel -y
# bzip2
yum install bzip2-devel
{% endhighlight %}

> 安装Python3

+ 下载并解压Python3源码
{% highlight console %}
wget https://www.python.org/ftp/python/3.4.2/Python-3.4.2.tgz
tar -zxvf Python-3.4.2.tgz
{% endhighlight %}

+ 使能ssl模块
{% highlight console %}
gedit Module/Setup
    # 修改以下内容
    # Socket module helper for socket(2)
    _socket socketmodule.c

    # Socket module helper for SSL support; you must comment out the other
    # socket line above, and possibly edit the SSL variable:
    #SSL=/usr/local/ssl
    _ssl _ssl.c \
        -DUSE_SSL -I$(SSL)/include -I$(SSL)/include/openssl \
        -L$(SSL)/lib -lssl -lcrypto
{% endhighlight %}

+ 开始安装
{% highlight console %}
mkdir /usr/local/python3.4
./configure --enable-shared --prefix=/usr/local/python3.4
make && make install
{% endhighlight %}

+ 修改链接
{% highlight console %}
cd /usr/bin
mv python python2
ln -s /usr/local/python3.4/bin/python3 python
gedit yum
    #将第一行修改为
    #!/usr/bin/python2
{% endhighlight %}

+ 安装pip
{% highlight console %}
wget https://pypi.python.org/packages/source/s/setuptools/setuptools-12.0.5.zip
unzip setuptools-12.0.5.zip
cd setuptools-12.0.5
python setup.py install
cd /usr/bin
mv easy_install easy_install_bak
ln -s /usr/local/python3.4/bin/easy_install easy_install
easy_install pip
{% endhighlight %}

> 安装Django
{% highlight console %}
pip install django
{% endhighlight %}

> 安装并配置Apache httpd

+ 安装apr
{% highlight console %}
wget http://mirror.symnds.com/software/Apache//apr/apr-1.5.1.tar.gz
tar -zxvf apr-1.5.1.tar.gz
cd apr-1.5.1
./configure --prefix=/usr/local/webserver/apr
make && make install
{% endhighlight %}

+ 安装apr-util
{% highlight console %}
wget http://mirror.symnds.com/software/Apache//apr/apr-util-1.5.4.tar.gz
tar -zxvf apr-util-1.5.4.tar.gz
cd apr-util-1.5.4
./configure --prefix=/usr/local/webserver/apr-util --with-apr=/usr/local/webserver/apr
make && make install
{% endhighlight %}

+ 安装pcre
{% highlight console %}
wget ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-8.36.tar.gz
tar -zxvf pcre-8.36.tar.gz
cd pcre-8.36
./configure
make && make install
{% endhighlight %}

+ 开始安装
{% highlight console %}
wget http://supergsego.com/apache//httpd/httpd-2.4.12.tar.gz
tar -zxvf httpd-2.4.12.tar.gz
cd httpd-2.4.12
./configure --prefix=/usr/local/webserver/httpd --sysconfdir=/usr/local/webserver/httpd/conf --enable-so --enable-rewrite --enable-ssl --enable-cgi --enable-cgid --enable-modules=most --enable-modules-shared=most --enable-mpms-shared=all --with-apr=/usr/local/webserver/apr --with-apr-util=/usr/local/webserver/apr-util
cp build/rpm/httpd.init /etc/init.d/httpd
make && make install
{% endhighlight %}

+ 部分配置
{% highlight console %}
gedit /etc/init.d/httpd
    # 修改以下内容
    httpd=${HTTPD-/usr/local/webserver/httpd/bin/httpd}
    pidfile=${PIDFILE-usr/local/webserver/httpd/logs/${prog}.pid}
    lockfile=${LOCKFILE-/var/lock/subsys/${prog}}
    RETVAL=0

    # check for 1.3 configuration
    check13 () {
        CONFFILE=/usr/local/webserver/httpd/conf/httpd.conf
    }
chmod 755 /etc/init.d/httpd
chkconfig --add httpd
cd /usr/sbin
ln -s /usr/local/webserver/httpd/bin/* .
ln -s /usr/local/webserver/httpd/logs /var/log/httpd
{% endhighlight %}

> 安装mod_wsgi
{% highlight console %}
gedit /etc/ld.so.conf.d/pythonlibs.conf
    # 在第一行添加
    /usr/local/python3.4/lib
reboot
wget https://github.com/GrahamDumpleton/mod_wsgi/archive/4.4.6.tar.gz
tar -zxvf 4.4.6.tar.gz
cd mod_wsgi-4.4.6
./configure
make && make install
{% endhighlight %}

> 配置httpd.conf
{% highlight console %}
gedit /usr/local/webserver/httpd/conf/httpd.conf
    # 修改
    ServerName your.server.name
    # 在LoadModule结尾处添加
    LoadModule wsgi_module modules/mod_wsgi.so
    # 在结尾处添加
    DocumentRoot "path/to/your/django_project"
    # add static file path
    Alias /static/ /path/to/your/static/
    <Directory /path/to/your/static/>
        Require all granted
    </Directory>
    # add wsgi.py file
    WSGIScriptAlias / /path/to/your/django_project/django_project/wsgi.py
    WSGIPythonPath /path/to/your/django_project/
    <Directory /path/to/your/django_project/django_project/>
        <Files wsgi.py>
            Require all granted
        </Files>
    </Directory>
{% endhighlight %}

至此，Django项目就可以运行在Apache httpd中了
