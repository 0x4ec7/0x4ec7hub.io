$(function () {
    adjust_footer_padding();
    $(window).on('resize', adjust_footer_padding);
    $(window).on('scroll', fix_nav_bar);
});

function adjust_footer_padding(){
    var window_height = $(window).height();
    var footer_offset_top = $('footer').offset().top;
    var footer_total_height = 96;
    if(footer_offset_top + footer_total_height >= window_height){
        $('footer').css('padding-bottom', 32);
    } else {
        $('footer').css('padding-bottom', 32 + window_height - footer_offset_top - footer_total_height);
    }
    fix_nav_bar();
}

function fix_nav_bar(){
    var window_height = $(window).height();
    var scroll_height = $(window).scrollTop();
    var footer_offset_top = $('footer').offset().top;
    var footer_total_height = 96;

    if(scroll_height > 143){
        $('.about-nav').css({
            'top' : 0,
            'bottom' : scroll_height + window_height - footer_offset_top,
            'height' : 'auto',
            'position' : 'fixed'
        });
    } else {
        $('.about-nav').css({
            'position' : 'absolute'
        });
    }
}